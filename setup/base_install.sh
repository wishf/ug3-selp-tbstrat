#!/bin/sh

# DEFAULT SETUP SCRIPT
# ASSUMES TANK SCRAP IS INSTALLED AND WE'RE IN THE REPOSITORY
# ALSO THAT THE VIRTUAL ENVIRONMENT IS ACTIVE
# PASS -d AS FIRST ARGUMENT TO ENABLE DEBUG
##############################################################

# Create DB and default config
tank_scrap init tank.db $1

# Create an admin user for the purposes of importing content
tank_scrap user add admin admin admin

# Run content import script
setup/import_default_content.sh admin

# Create test users
tank_scrap user add test test player
tank_scrap user add matt matt player
tank_scrap user add mari mari player

