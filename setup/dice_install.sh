#!/bin/sh

# DICE ENVIRONMENT SETUP SCRIPT
# Pass -d as first parameter to enable debug mode
#################################################

pyvenv venv

. venv/bin/activate

cd src

pip install -e .

cd ..

./setup/base_install.sh $1
