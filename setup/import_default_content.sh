#! /bin/sh

# Tiles
################################################
# ID 1
tank_scrap tile add content/tiles/grass.json
# ID 2
tank_scrap tile add content/tiles/beach.json
# ID 3
tank_scrap tile add content/tiles/mountain.json
# ID 4
tank_scrap tile add content/tiles/sea.json
################################################

# Units
################################################
# ID 1
tank_scrap unit add content/units/tank.json
################################################

# Maps
################################################
# ID 1
tank_scrap map add $1 content/maps/testmap01.json
# ID 2
tank_scrap map add $1 content/maps/terrain_test.json
################################################
