from setuptools import setup

setup(
    name='Tank-Scrap',
    version='0.0.1',
    description='A military-themed turn based strategy game',
    author='Matthew Summers',
    author_email='matt@wishf.co.uk',

    packages=['tank_scrap'],

    install_requires=[
        'flask',
        'flask-login',
        'flask-wtf',
        'flask-sqlalchemy',
        'Flask-Bootstrap',
        'Flask-Testing',
        'blinker',
        'passlib',
        'enum34',
        'nose'
    ],

    entry_points={
        'console_scripts': [
            'tank_scrap=tank_scrap.scripts.management.management:main',
        ],
    }
)
