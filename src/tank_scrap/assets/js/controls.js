$(function () {
  $('[data-is-unit="true"][data-players="True"]').bind('click', function (e) {
    $("table").find('div').attr('class', 'tint').unbind('click');

    $.getJSON($SCRIPT_ROOT + '/api/valid_moves', {
        unit: $(e.target).data("unit")
    }, function(data) {
        $.each(data.result, function(index, value) {
            if(value.move_type == "Move") {
                $("table").find('[data-x="' + value.x + '"][data-y="' + value.y + '"]').addClass("blue").bind('click', function(){
                    var dict = {};
                    dict["x"] = value.x;
                    dict["y"] = value.y;
                    dict["unit_id"] = $(e.target).data("unit");
                    dict["move"] = "Move";

                    $.ajax({
                        beforeSend: function(xhrObj){
                            xhrObj.setRequestHeader("Content-Type","application/json");
                            xhrObj.setRequestHeader("Accept","application/json");
                        },
                        type: "POST",
                        url: $SCRIPT_ROOT + '/api/submit_move',       
                        data: JSON.stringify(dict),               
                        dataType: "json",
                        success: move_success
                    });
                });
            } else if(value.move_type == "Attack") {
                $("table").find('[data-x="' + value.x + '"][data-y="' + value.y + '"]').addClass("red").bind('click', function(){
                    var dict = {};
                    dict["x"] = value.x;
                    dict["y"] = value.y;
                    dict["unit_id"] = $(e.target).data("unit");
                    dict["move"] = "Attack";

                    $.ajax({
                        beforeSend: function(xhrObj){
                            xhrObj.setRequestHeader("Content-Type","application/json");
                            xhrObj.setRequestHeader("Accept","application/json");
                        },
                        type: "POST",
                        url: $SCRIPT_ROOT + '/api/submit_move',       
                        data: JSON.stringify(dict),               
                        dataType: "json",
                        success: move_success
                    });
                });
            }
        });
    });
    return false;
  });
});

function move_success (json) {
    if(json.success == true) {
        $('h1').find('span').remove();
        location.reload(true)
    }
}
