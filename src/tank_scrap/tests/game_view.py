from flask.ext.testing import TestCase
from tank_scrap import db, init_app
import os
import tempfile

import tank_scrap.models as models

class GameViewTestCase(TestCase):

    render_templates = False

    def create_app(self):
        key = os.urandom(24)

        config = {
            'SQLALCHEMY_DATABASE_URI': 'sqlite://',
            'SECRET_KEY': str(key),
            'TESTING': True,
            'WTF_CSRF_ENABLED': False
        }

        app = init_app(config=config)

        return app

    def setUp(self):
        db.create_all()
        user = models.User('test', 'test')
        user2 = models.User('test2', 'test2')
        user3 = models.User('test3', 'test3')

        grass = models.TileType("Grass", 0,0,1,"grass",True,True,False)
        tank = models.UnitType("Tank", 0,0,0,3,20,10,10,0.25,2,"tank",True,False,False)

        _map = models.Map(
            'testmap',
            {
                'board': [
                    1,1,1,1,1,1,
                    1,1,1,1,1,1,
                    1,1,1,1,1,1,
                    1,1,1,1,1,1,
                    1,1,1,1,1,1,
                    1,1,1,1,1,1,
                    1,1,1,1,1,1
                ],
                'spawn_points': [
                    [
                        {
                            "unit": "Tank",
                            "x": 0,
                            "y": 3
                        }
                    ],
                    [
                        {
                            "unit": "Tank",
                            "x": 5,
                            "y": 3
                        }
                    ]
                ]
            },
            user,
            width=6,
            height=7
        )

        db.session.add(grass)
        db.session.add(tank)

        db.session.add(user)
        db.session.add(user2)
        db.session.add(user3)
        db.session.add(_map)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def login(self, username, password):
        return self.client.post('/login', data=dict(
            username=username,
            password=password
        ))

    def logout(self):
        return self.client.get('/logout')

    def create_game(self, player_ids, public=None):
        return self.client.post('/new-game', data=dict(
            name='testgame',
            game_map=1,
            other_players=player_ids,
            public=public,
            password='',
            confirm=''
        ))

    def view_game(self, id):
        return self.client.get('/game/{0}'.format(id))

    def test_create_game(self):
        self.login('test', 'test')
        resp = self.create_game([2])
        print(resp.status_code)
        self.assertRedirects(resp, '/game/1')

    def test_create_public_game(self):
        self.login('test', 'test')
        resp = self.create_game([2], public='y')
        self.assertRedirects(resp, '/game/1')

    def test_game_accessible(self):
        # Create the game as first user
        self.login('test', 'test')
        self.create_game([2])
        # Switch to second user who can view
        self.logout()
        self.login('test2', 'test2')
        resp = self.view_game(1)
        self.assertTemplateUsed('game.html')

    def test_game_inaccessible_anonymous(self):
        # Create the game as first user
        self.login('test', 'test')
        self.create_game([2])
        # Switch to anonymous user who cannot view
        self.logout()
        resp = self.view_game(1)
        self.assertRedirects(resp, '/login')

    def test_game_inaccessible_authenticated(self):
        # Create the game as first user
        self.login('test', 'test')
        self.create_game([2])
        # Switch to third user who cannot view
        self.logout()
        self.login('test3', 'test3')
        resp = self.view_game(1)
        self.assertRedirects(resp, '/')

    def test_public_game_accessible(self):
        # Create the game as first user
        self.login('test', 'test')
        self.create_game([2], public=True)
        # Switch to second user who can view
        self.logout()
        self.login('test2', 'test2')
        resp = self.view_game(1)
        self.assertTemplateUsed('game.html')

    def test_public_game_accessible_anonymous(self):
        # Create the game as first user
        self.login('test', 'test')
        self.create_game([2], public=True)
        # Switch to anonymous user who can view
        self.logout()
        resp = self.view_game(1)
        self.assertTemplateUsed('game.html')

    def test_public_game_accessible_authenticated(self):
        # Create the game as first user
        self.login('test', 'test')
        self.create_game([2], public=True)
        # Switch to third user who cann view
        self.logout()
        self.login('test3', 'test3')
        resp = self.view_game(1)
        self.assertTemplateUsed('game.html')
