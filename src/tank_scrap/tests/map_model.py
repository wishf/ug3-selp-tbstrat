import unittest
from tank_scrap import db, init_app
import os
import tempfile

import tank_scrap.models as models

class MapModelTestCase(unittest.TestCase):

    def setUp(self):
        key = os.urandom(24)

        config = {
            'SQLALCHEMY_DATABASE_URI': 'sqlite://',
            'SECRET_KEY': str(key),
            'TESTING': True
        }

        self.base_app = init_app(config=config)

        with self.base_app.test_request_context():
            db.create_all()

        self.app = self.base_app.test_client()

    def tearDown(self):
        pass

    def test_invariants(self):
        with self.base_app.test_request_context():
            fake_creator = models.User('test', 'test')
            db.session.add(fake_creator)

            _map = models.Map(
                'testmap',
                {
                    'board': [
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1
                    ],
                    'spawn_points': [
                        [
                            {
                                "unit": "Tank",
                                "x": 0,
                                "y": 3
                            }
                        ],
                        [
                            {
                                "unit": "Tank",
                                "x": 5,
                                "y": 3
                            }
                        ]
                    ]
                },
                fake_creator,
                width=6,
                height=7
            )

            # Confirm the length of the board
            assert(len(_map.board) == 6*7)

            # Confirm the length of the spawn list
            assert(len(_map.spawn_list) == 2)

            # Confirm it isn't possible to access out of bounds tiles
            self.assertRaises(Exception, lambda _: _map.get_tile(6,0))
            self.assertRaises(Exception, lambda _: _map.get_tile(-1, 0))
            self.assertRaises(Exception, lambda _: _map.get_tile(0, -1))
            self.assertRaises(Exception, lambda _: _map.get_tile(0, 8))

            # Confirm max teams is 2
            assert(_map.max_teams == 2)
