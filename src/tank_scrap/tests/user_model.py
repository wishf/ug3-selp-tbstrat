import unittest
from tank_scrap import db, init_app
import os
import tempfile

import tank_scrap.models as models

class UserModelTestCase(unittest.TestCase):

    def setUp(self):
        key = os.urandom(24)

        config = {
            'SQLALCHEMY_DATABASE_URI': 'sqlite://',
            'SECRET_KEY': str(key),
            'TESTING': True
        }

        self.base_app = init_app(config=config)

        with self.base_app.test_request_context():
            db.create_all()

        self.app = self.base_app.test_client()

    def tearDown(self):
        pass

    def test_no_users(self):
        with self.base_app.test_request_context():
            # If the DB is empty, this should return None
            users = models.User.query.first()

            assert(users is None)

    def test_user_invariants(self):
        with self.base_app.test_request_context():
            user = models.User('test', 'test')
            db.session.add(user)
            db.session.commit() # Flush to file

            # Check it is possible to query by name for this user
            exists = models.User.query.filter_by(name='test').first()

            assert(exists is not None)

            # Check non-existent users aren't found
            doesnt_exist = models.User.query.filter_by(name='toast').first()

            assert(doesnt_exist is None)

            # Check the correct password is accepted
            assert(user.verify('test') == True)

            # Chek wrong passwords are rejected
            assert(user.verify('toast') == False)

            # Check the winrate for a new user is zero
            assert(user.winrate == 0)

            # Check the number of games played is equal to the number of wins and losses
            assert(user.games_played == user.wins + user.losses)

            # Check that the number of games played for a new user is zero
            assert(user.games_played == 0)

            # Check that the wins and losses for a new player are both zero
            assert(user.wins == 0)
            assert(user.losses == 0)

            # Increment wins and check games played (confirm games played actually tracks wins)
            user.wins = 1
            assert(user.games_played == user.wins + user.losses)

            # Increment losses and check games played (confirm games played actually tracks losses)
            user.losses = 1
            assert(user.games_played == user.wins + user.losses)

            # Check winrate tracks the above statistics
            assert(user.winrate == 0.5)
            assert(user.winrate == (user.wins / user.games_played))


if __name__ == '__main__':
    unittest.main()