import unittest
from tank_scrap import db, init_app
import os
import tempfile

import tank_scrap.models as models
from sqlalchemy import distinct

class GameModelTestCase(unittest.TestCase):

    def setUp(self):
        key = os.urandom(24)

        config = {
            'SQLALCHEMY_DATABASE_URI': 'sqlite://',
            'SECRET_KEY': str(key),
            'TESTING': True
        }

        self.base_app = init_app(config=config)

        with self.base_app.test_request_context():
            db.create_all()
            user = models.User('test', 'test')
            user2 = models.User('test2', 'test2')
            user3 = models.User('test3', 'test3')

            grass = models.TileType("Grass", 0,0,1,"grass",True,True,False)
            tank = models.UnitType("Tank", 0,0,0,3,20,10,10,0.25,2,"tank",True,False,False)

            _map = models.Map(
                'testmap',
                {
                    'board': [
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1,
                        1,1,1,1,1,1
                    ],
                    'spawn_points': [
                        [
                            {
                                "unit": "Tank",
                                "x": 0,
                                "y": 3
                            }
                        ],
                        [
                            {
                                "unit": "Tank",
                                "x": 5,
                                "y": 3
                            }
                        ]
                    ]
                },
                user,
                width=6,
                height=7
            )

            db.session.add(grass)
            db.session.add(tank)

            db.session.add(user)
            db.session.add(user2)
            db.session.add(user3)
            db.session.add(_map)
            db.session.commit()

        self.app = self.base_app.test_client()

    def tearDown(self):
        pass

    def test_invariants(self):
        with self.base_app.test_request_context():
            _map = models.Map.query.first()
            creator = models.User.query.first()
            # Check it is impossible to make a one player game
            self.assertRaises(
                Exception,
                lambda _: models.Game('broken_game', [creator], _map, creator)
            )

            second_player = models.User.query.filter(models.User.name != creator.name).first()

            game = models.Game('testgame', [creator, second_player], _map, creator)

            # Check the creator is the first player
            assert(game.active_player == creator)

            # Check the game isn't already in an end state
            assert(game.is_victory == False)
            assert(game.is_draw == False)

            # Check a victor isn't already assigned
            assert(game.victor == None)

            # Check each player's team number is unique
            n_teams = db.session.query(models.Player.team_id).distinct().filter_by(game=game).count()

            assert(n_teams == 2)

            for player in [creator, second_player]:
                units = models.Unit.query.filter_by(owner=player, game=game).all()
                # Check each player has a unit
                assert(len(units) == 1)

                # Check that unit has full health, ammo and fuel
                assert(units[0].hp == 20)
                assert(units[0].ammo == 10)
                assert(units[0].fuel == 10)

                # Check that the units are correctly positioned
                if player == creator:
                    units[0].x = 0
                    units[0].y = 3
                else:
                    units[0].x = 5
                    units[0].y = 3

    def test_valid_moves(self):
        with self.base_app.test_request_context():
            _map = models.Map.query.first()
            creator = models.User.query.first()
            second_player = models.User.query.filter(models.User.name != creator.name).first()

            game = models.Game('testgame', [creator, second_player], _map, creator)

            unit = models.Unit.query.filter_by(owner=second_player).first()

            moves = set([(k.x, k.y, v) for k, v in game.get_valid_moves(unit)])
            print(moves)

            actual_moves = set([
                (5, 4, models.MoveType.move),
                (3, 3, models.MoveType.move),
                (4, 4, models.MoveType.move),
                (4, 3, models.MoveType.move),
                (4, 2, models.MoveType.move),
                (5, 1, models.MoveType.move),
                (5, 2, models.MoveType.move),
                (5, 5, models.MoveType.move)
            ])

            assert(moves == actual_moves)

