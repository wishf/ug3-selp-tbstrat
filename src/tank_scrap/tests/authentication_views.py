from flask.ext.testing import TestCase
from tank_scrap import db, init_app
import os
import tempfile

import tank_scrap.models as models

class AuthenticationViewsTestCase(TestCase):

    render_templates = False

    def create_app(self):
        key = os.urandom(24)

        config = {
            'SQLALCHEMY_DATABASE_URI': 'sqlite://',
            'SECRET_KEY': str(key),
            'TESTING': True,
            'WTF_CSRF_ENABLED': False
        }

        app = init_app(config=config)

        return app

    def setUp(self):
        db.create_all()
        user = models.User('test', 'test')

        db.session.add(user)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def login(self, username, password):
        return self.client.post('/login', data=dict(
            username=username,
            password=password
        ))

    def logout(self):
        return self.client.get('/logout')

    def test_login(self):
        # Check correct login works
        resp = self.login('test', 'test')
        self.assertRedirects(resp, '/')

    def test_incorrect_username(self):
        # Check incorrect username fails
        resp = self.login('toast', 'test')
        self.assertTemplateUsed('login.html')

    def test_incorrect_password(self):
        # Check incorrect password fails
        resp = self.login('test', 'toast')
        self.assertTemplateUsed('login.html')

    def test_logout(self):
        self.login('test', 'test')

        # Check logout works
        resp = self.logout()
        self.assertRedirects(resp, '/')

    def test_double_login_fails(self):
        self.login('test', 'test')

        # Check incorrect password fails
        resp = self.login('test', 'test')
        self.assertRedirects(resp, '/')
