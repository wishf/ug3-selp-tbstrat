from .user_model import UserModelTestCase
from .authentication_views import AuthenticationViewsTestCase
from .map_model import MapModelTestCase
from .game_view import GameViewTestCase
from .game_model import GameModelTestCase
