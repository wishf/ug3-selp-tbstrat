from flask import Flask, flash, redirect, url_for
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager, current_user
from flask_bootstrap import Bootstrap

db = SQLAlchemy()
login = LoginManager()

from tank_scrap.views.html import html
from tank_scrap.views.api import api
import tank_scrap.models as models


def init_app(pyfile=None, config=None):
    app = Flask(__name__,
                static_folder='assets')

    if pyfile:
        app.config.from_pyfile(pyfile, silent=True)

    if config:
        app.config.update(**config)

    db.init_app(app)
    login.init_app(app)
    Bootstrap(app)

    app.register_blueprint(html)
    app.register_blueprint(api, url_prefix='/api')

    @login.unauthorized_handler
    def unauth():
        if not current_user.get_id():
            flash("You need to be logged in to access that page", "danger")
            return redirect(url_for('html.login'))
        return redirect(url_for('html.index'))

    return app


@login.user_loader
def load_user(userid):
    return models.User.query.filter_by(id=userid).first()
