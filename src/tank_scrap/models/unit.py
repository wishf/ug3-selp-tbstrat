from tank_scrap import db


class Unit(db.Model):
    __tablenme__ = 'units'

    id = db.Column(db.Integer, primary_key=True)
    # The player in control of the unit
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    owner = db.relationship('User')

    # The game the unit is involved in
    game_id = db.Column(db.Integer, db.ForeignKey('games.id'), nullable=False)
    game = db.relationship('Game', backref=db.backref('units', lazy='dynamic'))

    # Where to inherit non-variable statistics (like max health, attack)
    unit_type_id = db.Column(
        db.Integer,
        db.ForeignKey('unittypes.id'),
        nullable=False)
    unit_type = db.relationship('UnitType')

    # Denotes unit variable statistics
    hp = db.Column(db.Integer, nullable=False)
    ammo = db.Column(db.Integer, nullable=False)
    fuel = db.Column(db.Integer, nullable=False)

    # Denotes unit position
    x = db.Column(db.Integer, nullable=False)
    y = db.Column(db.Integer, nullable=False)

    def __init__(self, owner, game, base, x, y):
        self.owner = owner
        self.game = game
        self.unit_type = base

        self.hp = base.hp
        self.ammo = base.ammo
        self.fuel = base.fuel

        self.x = x
        self.y = y

    @staticmethod
    def on_tile(x, y, game):
        return Unit.query.filter_by(x=x, y=y, game=game).first()
