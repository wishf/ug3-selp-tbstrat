from tank_scrap import db


class Player(db.Model):
    __tablename__ = 'players'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    game_id = db.Column(db.Integer, db.ForeignKey('games.id'))
    team_id = db.Column(db.Integer, nullable=False)
    disqualified = db.Column(db.Boolean, nullable=False)

    user = db.relationship(
        'User',
        backref=db.backref(
            'games_with_team',
            lazy='dynamic'))
    game = db.relationship(
        'Game',
        backref=db.backref(
            'players_with_team',
            lazy='dynamic'))

    def __init__(self, user, game, team):
        self.user = user
        self.game = game
        self.team_id = team
        self.disqualified = False

    def disqualify(self):
        self.disqualified = True
        self.user.losses = self.user.losses + 1
