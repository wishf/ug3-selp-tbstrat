from tank_scrap import db

from .tiletype import Passability

class UnitType(db.Model):
    __tablename__ = 'unittypes'

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(60), nullable=False)
    image_path = db.Column(db.String(256), nullable=False)

    # How far the unit can see
    vision_range = db.Column(db.Integer, nullable=False)
    # How far away the furthest unit this unit can hit
    max_firing_range = db.Column(db.Integer, nullable=False)
    # How far away the closest unit this unit can hit
    min_firing_range = db.Column(db.Integer, nullable=False)
    # How many tiles (unmodified) this unit can move in one turn
    movement_range = db.Column(db.Integer, nullable=False)

    # Base statistics for a unit of this type (hp, ammo and fuel are all maximums)
    hp = db.Column(db.Integer, nullable=False)
    ammo = db.Column(db.Integer, nullable=False)
    fuel = db.Column(db.Integer, nullable=False)
    damage = db.Column(db.Integer, nullable=False)
    armour = db.Column(db.Integer, nullable=False)

    # Denotes which types of tile a unit can pass through
    land_passing = db.Column(db.Boolean, nullable=False)
    air_passing = db.Column(db.Boolean, nullable=False)
    sea_passing = db.Column(db.Boolean, nullable=False)

    # Condenses passability values into a set, which we can then do subset
    # testing on
    @property
    def passability(self):
        if not hasattr(self, '_passability') or not self._passability:
            self._passability = set()
            if self.land_passing:
                self._passability.add(Passability.land)

            if self.air_passing:
                self._passability.add(Passability.air)

            if self.sea_passing:
                self._passability.add(Passability.sea)

        return self._passability

    def __init__(
            self,
            name,
            vision,
            max_f,
            min_f,
            movement,
            hp,
            ammo,
            fuel,
            damage,
            armour,
            img,
            land_passing,
            air_passing,
            sea_passing):
        self.name = name
        self.vision_range = vision
        self.max_firing_range = max_f
        self.min_firing_range = min_f
        self.movement_range = movement
        self.hp = hp
        self.ammo = ammo
        self.fuel = fuel
        self.damage = damage
        self.armour = armour
        self.image_path = img

        self.land_passing = land_passing
        self.air_passing = air_passing
        self.sea_passing = sea_passing

    @staticmethod
    def by_name(name):
        return UnitType.query.filter_by(name=name).first()
