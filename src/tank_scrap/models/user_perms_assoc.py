from tank_scrap import db

# Secondary table for a many-to-many relationship between users and permissions
user_perms_assoc = db.Table('user_perms_assoc', 
    db.Column('id', db.Integer, primary_key=True),
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('permission_id', db.Integer, db.ForeignKey('permissions.id'))
)
