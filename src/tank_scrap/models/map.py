from tank_scrap import db
import json
from .mapinternals.tile import Tile

from .unittype import UnitType
from .tiletype import TileType


class Map(db.Model):
    __tablename__ = 'maps'

    id = db.Column(db.Integer, primary_key=True)
    # We store the board as JSON
    _db_board = db.Column(db.Text, nullable=False)
    name = db.Column(db.String(255), nullable=False)

    creator_id = db.Column(
        db.Integer,
        db.ForeignKey('users.id'),
        nullable=False)
    creator = db.relationship('User')

    width = db.Column(db.Integer, nullable=False)
    height = db.Column(db.Integer, nullable=False)

    # Representation of map as a grid of tiles
    @property
    def board(self):
        if not hasattr(self, '_board') or not self._board:
            self._build_map()
        return self._board

    # Representation of map as a list of units which appear at the start
    @property
    def spawn_list(self):
        if not hasattr(self, '_spawn_list') or not self._spawn_list:
            self._build_spawnlist()
        return self._spawn_list

    @property
    def max_teams(self):
        if not hasattr(self, '_max_teams') or not self._max_teams:
            self._max_teams = 0
            for x, y, team, name in self.spawn_list:
                if team > self._max_teams:
                    self._max_teams = team

            # Account for the fact teams are zero indexed
            self._max_teams = self._max_teams + 1
        return self._max_teams

    @staticmethod
    def all():
        return Map.query.all()

    def get_tile(self, x, y):
        if y < 0 or y >= self.height:
            raise Exception("Y coordinate out of bounds")

        if x < 0 or x >= self.width:
            raise Exception("X coordinate out of bounds")

        return self.board[((y * self.width) + x)]

    # Construct the stored JSON in the database into a nicer-to-use
    # intermediate format. This solves spawn point lookup issues but at the
    # cost of some pre-processing and a bit of memory.
    def _build_map(self):
        json_board = json.loads(self._db_board)
        board = []
        for y in range(self.height):
            for x in range(self.width):
                ttype = TileType.query.filter_by(
                    id=json_board['board'][
                        (y * self.width) + x]).first()
                current = Tile(ttype, x, y)

                if x > 0:
                    current.add_neighbour(board[((y * self.width) + x - 1)])

                if y > 0:
                    current.add_neighbour(board[(((y - 1) * self.width) + x)])

                board.append(current)

        for i, team in enumerate(json_board['spawn_points']):
            for unit in team:
                x = unit['x']
                y = unit['y']
                board[((y * self.width) + x)].\
                    set_spawn(UnitType.by_name(unit['unit']), i)

        self._board = board

    # Construct the stored JSON into a nicer format for reading off unit spawn
    # locations
    def _build_spawnlist(self):
        json_board = json.loads(self._db_board)

        self._spawn_list = []
        for i, team in enumerate(json_board['spawn_points']):
            for unit in team:
                x = unit['x']
                y = unit['y']
                self._spawn_list.append(
                    (x,
                     y,
                     i,
                     UnitType.by_name(
                         unit['unit'])))

    def __init__(self, name, board, creator, width, height):
        self._db_board = json.dumps(board)
        self.name = name
        self.creator = creator
        self.width = width
        self.height = height
