from tank_scrap import db
from enum import Enum


class Passability(Enum):
    land = 1
    sea = 2
    air = 3

class TileType(db.Model):
    __tablenme__ = 'tiletypes'

    id = db.Column(db.Integer, primary_key=True)

    # The name of the tile and where to find it's graphic
    name = db.Column(db.String(60), nullable=False)
    image_path = db.Column(db.String(256), nullable=False)

    # How a unit's vision is affected by standing or moving through the tile
    stationary_vision_modifier = db.Column(db.Integer, nullable=False)
    mobile_vision_modifier = db.Column(db.Integer, nullable=False)

    # How a unit's movement is affected by the tile
    movement_modifier = db.Column(db.Integer, nullable=False)

    # Which types of unit the tile allows to pass through
    land_passable = db.Column(db.Boolean, nullable=False)
    air_passable = db.Column(db.Boolean, nullable=False)
    sea_passable = db.Column(db.Boolean, nullable=False)

    # Condenses passability values into a set, which we can then do subset
    # testing on
    @property
    def passability(self):
        if not hasattr(self, '_passability') or not self._passability:
            self._passability = set()
            if self.land_passable:
                self._passability.add(Passability.land)

            if self.air_passable:
                self._passability.add(Passability.air)

            if self.sea_passable:
                self._passability.add(Passability.sea)

        return self._passability
    

    def __init__(self, name, s_vision, m_vision, movement, img, land_passable, air_passable, sea_passable):
        self.name = name
        self.stationary_vision_modifier = s_vision
        self.mobile_vision_modifier = m_vision
        self.movement_modifier = movement
        self.image_path = img

        self.land_passable = land_passable
        self.air_passable = air_passable
        self.sea_passable = sea_passable
