from tank_scrap import db
from .user_perms_assoc import user_perms_assoc

from tank_scrap.security.crypto import password_context

from flask.ext.login import UserMixin

from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy import cast, case


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), nullable=False, unique=True)
    password = db.Column(db.String(130), nullable=False)
    wins = db.Column(db.Integer, nullable=False)
    losses = db.Column(db.Integer, nullable=False)

    permissions = db.relationship('Permission', secondary=user_perms_assoc)

    # The password parameter here should be a string, not an already-encrypted
    # password. Same applies for the verify method.
    def __init__(self, name, password):
        self.name = name
        self.password = password_context.encrypt(password)
        self.wins = 0
        self.losses = 0

    def verify(self, password):
        return password_context.verify(password, self.password)

    # This method provides a shorthand to the all method on the query member
    # of this class. The optional parameter 'excluding' expects a User object
    # and will filter out that user from the list of all users.
    @staticmethod
    def all(excluding=None):
        if excluding:
            return User.query.filter(User.id != excluding.id).all()
        return User.query.all()

    # Percentage of games played that were wins
    @hybrid_property
    def winrate(self):
        if self.games_played > 0:
            return self.wins / float(self.games_played)

        return 0

    @winrate.expression
    def winrate(self):
        return case(
            [(self.games_played > 0,
              self.wins / cast(self.games_played, db.Float)
            )],
            else_=0)

    @hybrid_property
    def games_played(self):
        return self.wins + self.losses

    @games_played.expression
    def games_played(self):
        return self.wins + self.losses

# Maintain old relationship with Game model now that we store team ID
# in the player model and it is not just a secondary table for a
# many-to-many relationship
User.games = association_proxy("games_with_team", "game")
