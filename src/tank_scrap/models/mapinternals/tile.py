class Tile:

    def __init__(self, tiletype, x, y, spawn=None, team=None):
        self.spawn_type = spawn
        self.spawn_team = team
        self.neighbours = []
        self.tiletype = tiletype
        self.x = x
        self.y = y

    def set_spawn(self, spawn, team):
        self.spawn_type = spawn
        self.spawn_team = team

    # This function adds a neighbour to the tile's list of neighbours
    # and if the neighbouring tile is said not to know about this tile
    # attempts to add it to it's list
    def add_neighbour(self, neighbour, unknown=True):
        self.neighbours.append(neighbour)
        if unknown:
            neighbour.add_neighbour(self, unknown=False)

    @property
    def movement_cost(self):
        return self.tiletype.movement_modifier

    def __hash__(self):
        return hash((self.x, self.y))

    # Check for equivalence between coordinates
    def __eq__(self, other):
        return hash(self) == hash(other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def passable(self, unit, current_cost, max_cost):
        cheap = self.movement_cost + current_cost < max_cost
        navigable = self.tiletype.passability >= unit.unit_type.passability

        return cheap and navigable


