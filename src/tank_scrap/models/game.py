from tank_scrap import db

from sqlalchemy.ext.associationproxy import association_proxy

from tank_scrap.security.crypto import password_context

from .unit import Unit
from .player import Player

import collections
from enum import Enum
from math import ceil


class MoveType(Enum):
    attack = 1
    move = 2

    def __repr__(self):
        if self == MoveType.attack:
            return "Attack"
        elif self == MoveType.move:
            return "Move"


class Game(db.Model):
    __tablename__ = 'games'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    # To be used for public lobbies
    password = db.Column(db.String(130))

    # The current player
    active_player_id = db.Column(
        db.Integer,
        db.ForeignKey('users.id'),
        nullable=False)
    active_player = db.relationship('User', foreign_keys=active_player_id)

    # The map being played on
    game_map_id = db.Column(
        db.Integer,
        db.ForeignKey('maps.id'),
        nullable=False)
    game_map = db.relationship('Map')

    # Is the game publically viewable
    public = db.Column(db.Boolean, nullable=False)

    # Has the game ended in a victory or a draw? (Both false if in progress)
    is_victory = db.Column(db.Boolean, nullable=False)
    is_draw = db.Column(db.Boolean, nullable=False)

    # The player which won
    victor_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    victor = db.relationship('User', foreign_keys=victor_id)

    def __init__(
            self,
            name,
            players,
            game_map,
            first_turn_player,
            password=None,
            public=False):
        if password:
            self.password = password_context.encrypt(password)

        if len(players) < 2:
            raise Exception("Needs at least two players")

        self.name = name
        self.game_map = game_map
        # Associate each player with a team in the order they came
        self.players_with_team = [
            Player(
                player,
                self,
                i) for i,
            player in enumerate(players)]
        self.active_player = first_turn_player
        self.public = public
        self.is_victory = False
        self.is_draw = False

        # For all the teams we have, spawn in their units
        # Ignore units that belong to teams which would not be controlled
        for x, y, team, unit in self.game_map.spawn_list:
            if team < len(self.players_with_team.all()):
                player = self.players_with_team.filter_by(team_id=team).first()
                current = Unit(player.user, self, unit, x, y)
                db.session.add(current)

    def verify(self, password):
        return password_context.verify(password, self.password)

    # Performs breadth-first search to find all possible paths less
    # than a given cost. Currently, this assumes all units can only
    # attack units adjacent to them - it is intended that a secondary
    # search function exist to get attack moves, and this can account
    # for longer-ranged or minimum-range attacks
    def get_valid_moves(self, unit):
        max_cost = unit.unit_type.movement_range
        frontier = collections.deque()
        start = self.game_map.get_tile(unit.x, unit.y)
        frontier.append(start)

        visited = {}
        visited[start] = True

        cost = {}
        cost[start] = 0

        while len(frontier) != 0:
            current = frontier.popleft()
            for next in current.neighbours:
                if next not in visited:
                    # If the tile is less than the maximum cost and passable
                    # we might be able to move there
                    if next.passable(unit, cost[current], max_cost):
                        # If there's a unit on the tile, we can attack it
                        # Else we can move there
                        if not Unit.on_tile(next.x, next.y, self):
                            frontier.append(next)
                            visited[next] = MoveType.move
                            cost[next] = cost[current] + next.movement_cost
                        else:
                            visited[next] = MoveType.attack

                    else:
                        # We're on a tile adjacent to one which is less than
                        # maximum cost, but it's too costly to move there.
                        # If there's a unit on this tile, we can attack it
                        # Otherwise we just note we've visited it and it's
                        # pointless to search it's neighbours
                        if Unit.on_tile(next.x, next.y, self):
                            visited[next] = MoveType.attack
                        else:
                            visited[next] = True

        # Strip out all the ones we simply visited to get a list of possible
        # moves
        return [(k,v) for k,v in visited.items() if v != True]

    # This is a slightly modified version of the breadth first search above. It
    # searches the same space, checking for equality wit the goal tile, and
    # returning the type of move when it is done.
    def is_valid_move(self, unit, goal):
        max_cost = unit.unit_type.movement_range
        frontier = collections.deque()
        start = self.game_map.get_tile(unit.x, unit.y)
        frontier.append(start)

        visited = {}
        visited[start] = True

        cost = {}
        cost[start] = 0

        while len(frontier) != 0:
            current = frontier.popleft()
            for next in current.neighbours:
                if next not in visited:
                    # If the tile we're looking at is below max cost, we can
                    # move to it if it is also passable. 
                    # If there's a unit on it, we can attack that instead
                    if next.passable(unit, cost[current], max_cost):
                        if not Unit.on_tile(next.x, next.y, self):
                            if next == goal:
                                return True, MoveType.move
                            frontier.append(next)
                            visited[next] = True
                            cost[next] = cost[current] + next.movement_cost
                        else:
                            if next == goal:
                                return True, MoveType.attack
                            visited[next] = True
                    else:
                        if Unit.on_tile(next.x, next.y, self):
                            if next == goal:
                                return True, MoveType.attack
                            visited[next] = True
                        else:
                            visited[next] = True

        return False, "None"

    # TODO: See if we can merge this with one of the other search functions to
    # improve code reuse
    def find_closest_valid_move(self, unit, goal):
        start = self.game_map.get_tile(unit.x, unit.y)

        if goal in start.neighbours:
            return start

        max_cost = unit.unit_type.movement_range
        frontier = collections.deque()
        frontier.append(start)

        visited = {}
        visited[start] = True

        cost = {}
        cost[start] = 0

        while len(frontier) != 0:
            current = frontier.popleft()
            for next in current.neighbours:
                if next not in visited:
                    if next.passable(unit, cost[current], max_cost):
                        if not Unit.on_tile(next.x, next.y, self):
                            if goal in next.neighbours:
                                return next
                            frontier.append(next)
                            visited[next] = True
                            cost[next] = cost[current] + next.movement_cost

        return None

    # Check the victory state of the game: in-progress, draw, or victory.
    # Disqualify players which are no longer capable of qualifying for the
    # victory condition and award them a loss. Award players who qualify for
    # victory a win
    def check_victory_condition(self):
        for player in self.players_with_team:
            # Check each player has at least one unit
            has_unit = Unit.query.filter_by(owner=player.user, game=self).first()

            # If there are no units left, player is disqualified
            if not has_unit:
                player.disqualify()
                db.session.add(player)

        remaining = self.players_with_team.filter_by(disqualified=False).all()
        # If all players are disqualified, we have a draw
        if len(remaining) == 0:
            self.is_draw = True
        # If only one is left standing, they are the winner
        elif len(remaining) == 1:
            self.is_victory = True
            self.victor = remaining[0].user
            self.victor.wins = self.victor.wins + 1
            db.session.add(self.victor)

        db.session.add(self)
        db.session.commit() 

    # Change the active player so that another player may input their turn
    # instead. 
    def switch_turns(self):
        # Advance the player to the one with the next numeric team ID
        teams = len(self.players_with_team.all())
        current = self.players_with_team.\
                    filter_by(user=self.active_player).\
                    first().team_id
        next = (current+ 1) % teams

        self.active_player = self.players_with_team.\
                                filter_by(team_id=next).\
                                first().user

        db.session.add(self)
        db.session.commit()

    # Take a turn given a unit, the move type the user submitted, and the
    # location the move should take place
    def take_turn(self, unit, recieved_movetype, x, y):
        # This person is trying to submit a turn out of order; disregard
        if unit.owner != self.active_player:
            return False

        # Convert the recieved move type into one of the enum values
        if recieved_movetype == "Attack":
            recieved_movetype = MoveType.attack
        elif recieved_movetype == "Move":
            recieved_movetype = MoveType.move
        else:
            # We've been sent garbage; tell them to go away
            return False

        target = self.game_map.get_tile(x, y)
        valid, movetype = self.is_valid_move(unit, target)

        # If the user has sent us bogus data, reject it
        if not valid or not movetype == recieved_movetype:
            return False

        # Perform an attack
        if movetype == MoveType.attack:
            # determine tile closest to target in move range
            move = self.find_closest_valid_move(unit, target)

            target_unit = Unit.on_tile(target.x, target.y, unit.game)

            # Give unit it's new location
            unit.x = move.x
            unit.y = move.y

            # Compute target HP first so that attacking unit gets advantage
            # Formula is:
            # HP = Ceil(OldHP - Max((AttackerHP * AttackerDamage) - TargetArmour))
            target_unit.hp = ceil(target_unit.hp - max(0,(unit.hp * unit.unit_type.damage) - target_unit.unit_type.armour))
            unit.hp = ceil(unit.hp - max(0,(target_unit.hp * target_unit.unit_type.damage) - unit.unit_type.armour))

            # Delete dead targets
            if target_unit.hp <= 0:
                db.session.delete(target_unit)
            else:
                db.session.add(target_unit)

            # Delete dead attackers
            if unit.hp <= 0:
                db.session.delete(unit)
            else:
                db.session.add(unit)

        # Move unit into place
        elif movetype == MoveType.move:
            unit.x = target.x
            unit.y = target.y
        
            db.session.add(unit)

        db.session.commit()

        # See if anyone is disqualified or anyone has won
        self.check_victory_condition()

        if not self.is_victory and not self.is_draw:
            # Advance turn
            self.switch_turns()

        return True

Game.players = association_proxy("players_with_team", "user")
