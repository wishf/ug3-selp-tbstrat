from flask import flash


def flash_errors(field):
    if field.errors:
        for error in field.errors:
            flash(error, 'danger')
