# This file defines the API blueprint
# This allows us to easily swap out API implementations as well as
# allowing us to keep API code cleanly seperated from the HTML code

from flask import Blueprint

from .valid_moves import ValidMovesView
from .submit_move import SubmitMoveView
from .check_turn import CheckTurnView

api = Blueprint('api', __name__)

api.add_url_rule(
    '/valid_moves',
    view_func=ValidMovesView.as_view('valid_moves')
)

api.add_url_rule(
    '/submit_move',
    view_func=SubmitMoveView.as_view('submit_move')
)

api.add_url_rule(
    '/check_turn',
    view_func=CheckTurnView.as_view('check_turn')
)
