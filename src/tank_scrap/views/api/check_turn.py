from flask.views import MethodView

from flask import jsonify, request, abort

import tank_scrap.models as models
from tank_scrap import db


class CheckTurnView(MethodView):

    def get(self):
        user_id = request.args.get('user')
        game_id = request.args.get('game')

        if not user_id or not game_id:
            return abort(404)

        user = models.User.query.filter_by(id=user_id).first()

        if not user:
            return abort(404)

        game = models.Game.query.filter_by(id=game_id).first()

        if not game:
            return abort(404)

        if user not in game.players:
            return abort(404)

        return jsonify(result=(user == game.active_player))



