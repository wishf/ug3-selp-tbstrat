from flask.views import MethodView

from flask import jsonify, request, abort

import tank_scrap.models as models
from tank_scrap import db


class SubmitMoveView(MethodView):

    def post(self):
        json = request.get_json()

        unit = models.Unit.query.filter_by(id=json['unit_id']).first()

        if not unit:
            return abort(404)

        success = unit.game.take_turn(unit, json['move'], json['x'], json['y'])

        return jsonify(success=success)


