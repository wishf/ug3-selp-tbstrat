from flask.views import MethodView

from flask import jsonify, request, abort

import tank_scrap.models as models


class ValidMovesView(MethodView):

    def get(self):
        unit_id = request.args.get('unit')

        if not unit_id:
            return abort(404)

        unit = models.Unit.query.filter_by(id=unit_id).first()

        if not unit:
            return abort(404)

        moves = unit.game.get_valid_moves(unit)

        return jsonify(result=[{'move_type': repr(v), 'x': k.x, 'y': k.y} for k, v in moves])


