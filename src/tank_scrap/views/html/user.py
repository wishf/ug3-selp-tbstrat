from flask.views import MethodView

from flask import render_template, abort

import tank_scrap.models as models


class UserView(MethodView):

    def get(self, id):
        user = models.User.query.filter_by(id=id).first()

        if not user:
            return abort(404)

        return render_template('user.html', user=user)
