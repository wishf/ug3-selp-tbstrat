from flask.views import View
from flask import render_template, url_for, redirect, flash, current_app

from .forms.register import RegisterForm
from flask.ext.login import current_user

from tank_scrap.util.form_helpers import flash_errors
import tank_scrap.models as models
from tank_scrap import db


class RegisterView(View):
    methods = ['GET', 'POST']

    def dispatch_request(self):
        # Send registered users to the homepage
        if current_user.get_id() is not None:
            return current_app.login_manager.unauthorized()

        form = RegisterForm()

        if form.validate_on_submit():
            user = models.User.query.filter_by(name=form.username.data).first()

            if not user:
                user = models.User(form.username.data, form.password.data)
                db.session.add(user)
                db.session.commit()

                flash('User account created', 'success')
                return redirect(url_for('html.login'))
            else:
                flash('That username is taken', 'danger')

        flash_errors(form.username)
        flash_errors(form.password)

        return render_template('register.html', form=form)
