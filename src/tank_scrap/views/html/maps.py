from flask.views import MethodView

from flask.ext.login import current_user

from flask import render_template, current_app, flash

import tank_scrap.models as models


class MapsView(MethodView):

    def get(self, id):
        # Pull out 20 maps per page
        maps = models.Map.query.paginate(id)

        return render_template('maps.html', maps=maps)
