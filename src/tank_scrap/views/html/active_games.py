from flask.views import MethodView

from flask.ext.login import current_user

from flask import render_template, current_app, flash


class ActiveGamesView(MethodView):

    def get(self):
        # Send non-registered users to the homepage
        if not current_user.get_id():
            return current_app.login_manager.unauthorized()

        return render_template('active_games.html', user=current_user)
