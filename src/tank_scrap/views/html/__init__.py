
# This file is for the HTML frontend.
# We define a blueprint for all the endpoints this provides.
# This allows us to define seperate blueprints for APIs and other possible
# frontend configurations in a modular way.

from flask import Blueprint

from .login import LoginView
from .logout import LogoutView
from .register import RegisterView
from .leaderboard import LeaderboardView
from .index import IndexView
from .game import GameView
from .user import UserView
from .create_game import CreateGameView
from .map import MapView
from .tile import TileView
from .active_games import ActiveGamesView
from .maps import MapsView

html = Blueprint('html', __name__,
                 template_folder='templates')

html.add_url_rule(
    '/',
    view_func=IndexView.as_view('index')
)
html.add_url_rule(
    '/login',
    view_func=LoginView.as_view('login')
)
html.add_url_rule(
    '/logout',
    view_func=LogoutView.as_view('logout')
)
html.add_url_rule(
    '/register',
    view_func=RegisterView.as_view('register')
)
html.add_url_rule(
    '/leaderboard',
    view_func=LeaderboardView.as_view('leaderboard')
)

# Since there can be multiple games with the same name, we avoid clashes by
# using the database-assigned unique ID
html.add_url_rule(
    '/game/<int:id>',
    view_func=GameView.as_view('game')
)

html.add_url_rule(
    '/map/<int:id>',
    view_func=MapView.as_view('map')
)
html.add_url_rule(
    '/css/tiles.css',
    view_func=TileView.as_view('tiles')
)

# We sacrifice human interpretability in this route in order to avoid issues
# involving unicode characters in user names, as well as possibly large urls
html.add_url_rule(
    '/user/<int:id>',
    view_func=UserView.as_view('user')
)

html.add_url_rule(
    '/new-game',
    view_func=CreateGameView.as_view('create_game')
)
html.add_url_rule(
    '/active-games',
    view_func=ActiveGamesView.as_view('active_games')
)
html.add_url_rule(
    '/maps/<int:id>',
    view_func=MapsView.as_view('map_list')
)
