from flask.views import MethodView

from flask import render_template, make_response

import tank_scrap.models as models


class TileView(MethodView):

    def get(self):
        tiles = models.TileType.query.all()
        headers = {'Content-Type': 'text/css'}
        return make_response(
            render_template('tile.css', tiles=tiles),
            200,
            headers)
