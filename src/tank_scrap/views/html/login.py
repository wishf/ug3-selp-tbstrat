from flask.views import View
from flask import render_template, url_for, redirect, flash, current_app
from flask.ext.login import login_user, current_user

from .forms.login import LoginForm

from tank_scrap.util.form_helpers import flash_errors
import tank_scrap.models as models


class LoginView(View):
    methods = ['GET', 'POST']

    def dispatch_request(self):
        # Send registered users to the homepage
        if current_user.get_id() is not None:
            return current_app.login_manager.unauthorized()

        form = LoginForm()

        if form.validate_on_submit():
            user = models.User.query.filter_by(name=form.username.data).first()

            # If the user exists and has the correct password, log them in
            if user and user.verify(form.password.data):
                login_user(user, remember=False)
                flash('Logged in successfully', 'success')
                return redirect(url_for('html.index'))
            else:
                flash('Invalid username/password combination', 'danger')

        flash_errors(form.username)
        flash_errors(form.password)

        return render_template('login.html', form=form)
