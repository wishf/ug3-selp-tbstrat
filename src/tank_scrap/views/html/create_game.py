from flask.views import View

from flask import render_template, current_app, redirect, url_for, flash

from flask.ext.login import current_user

from .forms.create_game import CreateGameForm

from tank_scrap.util.form_helpers import flash_errors
import tank_scrap.models as models
from tank_scrap import db


class CreateGameView(View):
    methods = ['GET', 'POST']

    def dispatch_request(self):
        # Send non-registered users to the homepage
        if not current_user.get_id():
            return current_app.login_manager.unauthorized()

        form = CreateGameForm()

        if form.validate_on_submit():
            try:
                game = models.Game(
                    form.name.data,
                    form.other_players.data + [current_user],
                    form.game_map.data,
                    current_user,
                    password=form.password.data,
                    public=form.public.data
                )

                db.session.add(game)
                db.session.commit()

                return redirect(url_for('html.game', id=game.id))
            except Exception:
                flash("You must select at least one other player", "danger")

        flash_errors(form.name)
        flash_errors(form.password)
        flash_errors(form.game_map)
        flash_errors(form.other_players)

        return render_template('create_game.html', form=form)
