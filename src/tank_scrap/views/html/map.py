from flask.views import MethodView

from flask import render_template, abort

import tank_scrap.models as models


class MapView(MethodView):

    def get(self, id):
        _map = models.Map.query.filter_by(id=id).first()

        # 404 if there is no map
        if not _map:
            return abort(404)

        # Give a colour mapping to teams so we can render their units
        colour_mapping = ['red', 'blue']

        return render_template('map.html',
                               map=_map,
                               colours=colour_mapping,
                               unit_loader=models.UnitType.by_name)
