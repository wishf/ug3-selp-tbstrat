from flask.views import MethodView

from flask import url_for, redirect, flash

from flask.ext.login import logout_user


class LogoutView(MethodView):

    def get(self):
        logout_user()
        flash('You were successfully logged out', 'success')
        return redirect(url_for('html.index'))
