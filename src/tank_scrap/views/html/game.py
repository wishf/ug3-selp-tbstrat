from flask.views import MethodView

from flask import render_template, current_app, abort

from flask.ext.login import current_user

import tank_scrap.models as models


class GameView(MethodView):

    def get(self, id):
        game = models.Game.query.filter_by(id=id).first()

        # 404 if the game doesn't exist
        if not game:
            return abort(404)

        # Send non-logged in users to the homepage if the game isn't public
        # Send non-participants to the homepage for the same reason
        if not game.public and not current_user in game.players:
            return current_app.login_manager.unauthorized()

        # The colours to assign teams - this is a temporary assignment
        # Ideally we'd have a better system for picking your own team
        # colours
        colour_mapping = ['red', 'blue']

        return render_template(
            'game.html',
            game=game,
            map=game.game_map,
            colours=colour_mapping,
            unit_on_tile=models.Unit.on_tile)
