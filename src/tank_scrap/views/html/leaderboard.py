from flask.views import MethodView
from flask import render_template, url_for

import tank_scrap.models as models
from tank_scrap import db


class LeaderboardView(MethodView):

    def get(self):
        top50 = models.User.query.filter(models.User.games_played > 10).\
            order_by(models.User.winrate.desc()).\
            limit(50)
        return render_template('leaderboard.html',
                               users=top50)
