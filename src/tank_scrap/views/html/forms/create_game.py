from flask_wtf import Form
from wtforms import StringField, PasswordField, BooleanField
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import DataRequired, EqualTo

from flask.ext.login import current_user

import tank_scrap.models as models


def all_users_except_current():
    return models.User.all(excluding=current_user)


class CreateGameForm(Form):
    name = StringField('Name', validators=[
        DataRequired(message='No username given')
    ])
    password = PasswordField('Password', validators=[
        EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    game_map = QuerySelectField(
        'Select map',
        query_factory=models.Map.all,
        get_label='name',
        allow_blank=False)
    # This field is used as a temporary measure - ideally we would have some
    # sort of friend's list functionality, or a better way to search users
    # (there seems to be several nice JS libraries which would do this), but
    # putting time into making the UI cleaner at the expense of the game system
    # was deemed not a good use of time.
    other_players = QuerySelectMultipleField(
        'Select players',
        query_factory=all_users_except_current,
        get_label='name',
        allow_blank=False)
    public = BooleanField('Publically visible?')
