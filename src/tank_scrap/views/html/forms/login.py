from flask_wtf import Form
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired


class LoginForm(Form):
    username = StringField('Username', validators=[
        DataRequired(message='No username given')
    ])
    password = PasswordField('Password', validators=[
        DataRequired(message='No password given')
    ])
