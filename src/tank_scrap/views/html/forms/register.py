from flask_wtf import Form
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, EqualTo


class RegisterForm(Form):
    username = StringField('Username', validators=[
        DataRequired(message='No username given')
    ])
    password = PasswordField('Password', validators=[
        DataRequired(message='No password given'),
        EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
