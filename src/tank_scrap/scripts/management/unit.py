import json
from tank_scrap import db, init_app
import tank_scrap.models as models


def import_unit(unit_filename, config):
    with open(unit_filename, 'r') as content:
        raw = content.read()

    parsed = json.loads(raw)

    # TODO: Input validation
    name = parsed['name']
    vision = parsed['vision_range']
    max_firing = parsed['max_firing_range']
    min_firing = parsed['min_firing_range']
    movement = parsed['movement_range']
    hp = parsed['hp']
    ammo = parsed['ammo']
    fuel = parsed['fuel']
    image = parsed['image_path']
    damage = parsed['damage']
    armour = parsed['armour']

    land_passing = parsed['land_passing']
    air_passing = parsed['air_passing']
    sea_passing = parsed['sea_passing']

    app = init_app(pyfile=config)

    with app.test_request_context():

        parsed_unit = models.UnitType(
            name,
            vision,
            max_firing,
            min_firing,
            movement,
            hp,
            ammo,
            fuel,
            damage,
            armour,
            image,
            land_passing,
            air_passing,
            sea_passing)
        db.session.add(parsed_unit)
        db.session.commit()
        print("Unit \'{0}\' added with ID {1}".format(name, parsed_unit.id))


def remove_unit(unit_name, config):
    app = init_app(pyfile=config)

    with app.test_request_context():
        exists = models.UnitType.query.filter_by(name=unit_name)

        if not exists:
            print("No such unit exists")
            return None

        db.session.delete(exists)
        db.session.commit()
        print("Unit removed")
