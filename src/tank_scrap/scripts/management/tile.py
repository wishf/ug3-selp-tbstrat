import json
from tank_scrap import db, init_app
import tank_scrap.models as models


def import_tile(tile_filename, config):
    with open(tile_filename, 'r') as content:
        raw = content.read()

    parsed = json.loads(raw)

    # TODO: Input validation
    name = parsed['name']
    stat_vision = parsed['stationary_vision_modifier']
    mobile_vision = parsed['mobile_vision_modifier']
    movement = parsed['movement_modifier']
    image = parsed['image_path']

    land_passable = parsed['land_passable']
    air_passable = parsed['air_passable']
    sea_passable = parsed['sea_passable']

    app = init_app(pyfile=config)

    with app.test_request_context():

        parsed_tile = models.TileType(
            name,
            stat_vision,
            mobile_vision,
            movement,
            image,
            land_passable,
            air_passable,
            sea_passable)
        db.session.add(parsed_tile)
        db.session.commit()
        print("Tile \'{0}\' added with ID {1}".format(name, parsed_tile.id))


def remove_tile(tile_name, config):
    app = init_app(pyfile=config)

    with app.test_request_context():
        exists = models.TileType.query.filter_by(name=tile_name)

        if not exists:
            print("No such tile exists")
            return None

        db.session.delete(exists)
        db.session.commit()
        print("Tile removed")
