from tank_scrap import db, init_app


def init_db(filename):
    app = init_app(pyfile=filename)

    with app.test_request_context():
        db.create_all()
