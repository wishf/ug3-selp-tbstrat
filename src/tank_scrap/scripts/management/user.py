from tank_scrap import db, init_app
import tank_scrap.models as models


def add_user(filename, name, pwd):
    app = init_app(pyfile=filename)

    with app.test_request_context():
        exists = models.User.query.filter_by(name=name).first()

        if exists:
            print("User already exists")
            return None

        user = models.User(name, pwd)

        db.session.add(user)
        db.session.commit()
        print("User \'{0}\' added with ID {1}".format(user.name, user.id))


def verify_user(filename, name, pwd):
    app = init_app(pyfile=filename)

    with app.test_request_context():
        exists = models.User.query.filter_by(name=name).first()

        if not exists:
            print("User does not exist")
            return None

        if exists.verify(pwd):
            print("Specified user has the entered password")
        else:
            print("Specified user does not have the entered password")


def remove_user(filename, name):
    app = init_app(pyfile=filename)

    with app.test_request_context():
        exists = models.User.query.filter_by(name=name).first()

        if not exists:
            print("User does not exist")
            return None

        db.session.delete(exists)
        db.session.commit()
        print("User removed")
