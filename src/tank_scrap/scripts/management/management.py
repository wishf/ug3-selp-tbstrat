import argparse

from .config import gen_config
from .database import init_db
from .user import add_user, remove_user, verify_user
from .map import import_map, remove_map
from .unit import import_unit, remove_unit
from .tile import import_tile, remove_tile

from tank_scrap import init_app
import os.path


def setup_parser():
    parser = argparse.ArgumentParser()

    subcommands = parser.add_subparsers()

    init_parser = subcommands.add_parser('init')

    init_parser.add_argument('dbfile')

    init_parser.add_argument(
        'db_scheme',
        help='The DB provider (e.g. sqlite)',
        choices=['sqlite', 'postgresql', 'mysql'],
        default='sqlite',
        nargs='?'
    )

    init_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to output the generated configuration to',
        nargs='?'
    )

    init_parser.add_argument(
        '--debug', '-d',
        help='Toggles debug mode in the configuration',
        action='store_true'
    )
    init_parser.set_defaults(func=init)

    user_parser = subcommands.add_parser('user')

    user_subcommands = user_parser.add_subparsers()

    user_add_parser = user_subcommands.add_parser('add')
    user_add_parser.add_argument('name')
    user_add_parser.add_argument('password')
    user_add_parser.add_argument('rights', choices=['admin', 'player'])
    user_add_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to read the configuration from',
        nargs='?'
    )
    user_add_parser.set_defaults(func=user_add)

    user_remove_parser = user_subcommands.add_parser('remove')
    user_remove_parser.add_argument('name')
    user_remove_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to read the configuration from',
        nargs='?'
    )
    user_remove_parser.set_defaults(func=user_remove)

    user_verify_parser = user_subcommands.add_parser('verify')
    user_verify_parser.add_argument('name')
    user_verify_parser.add_argument('password')
    user_verify_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to read the configuration from',
        nargs='?'
    )
    user_verify_parser.set_defaults(func=user_verify)

    map_parser = subcommands.add_parser('map')

    map_subcommands = map_parser.add_subparsers()

    map_add_parser = map_subcommands.add_parser('add')
    map_add_parser.add_argument('creator')
    map_add_parser.add_argument('json')
    map_add_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to read the configuration from',
        nargs='?'
    )
    map_add_parser.set_defaults(func=map_add)

    map_remove_parser = map_subcommands.add_parser('remove')
    map_remove_parser.add_argument('name')
    map_remove_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to read the configuration from',
        nargs='?'
    )
    map_remove_parser.set_defaults(func=map_remove)

    unit_parser = subcommands.add_parser('unit')

    unit_subcommands = unit_parser.add_subparsers()

    unit_add_parser = unit_subcommands.add_parser('add')
    unit_add_parser.add_argument('json')
    unit_add_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to read the configuration from',
        nargs='?'
    )
    unit_add_parser.set_defaults(func=unit_add)

    unit_remove_parser = map_subcommands.add_parser('remove')
    unit_remove_parser.add_argument('name')
    unit_remove_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to read the configuration from',
        nargs='?'
    )
    unit_remove_parser.set_defaults(func=unit_remove)

    tile_parser = subcommands.add_parser('tile')

    tile_subcommands = tile_parser.add_subparsers()

    tile_add_parser = tile_subcommands.add_parser('add')
    tile_add_parser.add_argument('json')
    tile_add_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to read the configuration from',
        nargs='?'
    )
    tile_add_parser.set_defaults(func=tile_add)

    tile_remove_parser = map_subcommands.add_parser('remove')
    tile_remove_parser.add_argument('name')
    tile_remove_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to read the configuration from',
        nargs='?'
    )
    tile_remove_parser.set_defaults(func=tile_remove)

    run_parser = subcommands.add_parser('run')

    run_parser.add_argument(
        'filename',
        default='config.py',
        help='The file to use for runtime configuration',
        nargs='?'
    )

    run_parser.set_defaults(func=run)

    return parser


def init(namespace):
    print("Generating configuration file...")
    gen_config(namespace.db_scheme,
               namespace.dbfile,
               namespace.filename,
               namespace.debug)

    print("Initialising database...")
    init_db(namespace.filename)


def user_add(namespace):
    add_user(namespace.filename, namespace.name, namespace.password)


def user_remove(namespace):
    remove_user(namespace.filename, namespace.name)


def user_verify(namespace):
    verify_user(namespace.filename, namespace.name, namespace.password)


def map_add(namespace):
    import_map(namespace.json, namespace.creator, namespace.filename)


def map_remove(namespace):
    remove_map(namespace.name, namespace.filename)


def tile_add(namespace):
    import_tile(namespace.json, namespace.filename)


def tile_remove(namespace):
    remove_tile(namespace.name, namespace.filename)


def unit_add(namespace):
    import_unit(namespace.json, namespace.filename)


def unit_remove(namespace):
    remove_unit(namespace.name, namespace.filename)


def run(namespace):
    print("Launching instance...")
    confpath = os.path.abspath(namespace.filename)

    app = init_app(pyfile=confpath)
    app.run()


def main():
    parser = setup_parser()
    namespace = parser.parse_args()

    namespace.filename = os.path.abspath(namespace.filename)

    namespace.func(namespace)
