import json
from tank_scrap import db, init_app
import tank_scrap.models as models


def import_map(map_filename, creator_name, config_filename):
    with open(map_filename, 'r') as content:
        raw = content.read()

    parsed = json.loads(raw)

    # TODO: Input validation
    name = parsed['name']
    width = parsed['width']
    height = parsed['height']
    data = parsed['data']

    app = init_app(pyfile=config_filename)

    with app.test_request_context():
        creator = models.User.query.filter_by(name=creator_name).first()

        if not creator:
            print("No users with the given name were found")
            return None

        parsed_map = models.Map(name, data, creator, width, height)
        db.session.add(parsed_map)
        db.session.commit()
        print("Map \'{0}\' added with ID {1}".format(name, parsed_map.id))


def remove_map(map_name, config_filename):
    app = init_app(pyfile=config_filename)

    with app.test_request_context():
        exists = models.Map.query.filter_by(name=map_name)

        if not exists:
            print("No such map exists")
            return None

        db.session.delete(exists)
        db.session.commit()
        print("Map removed")
