\documentclass[11pt,a4paper]{scrreprt}

\setlength{\parskip}{2mm}

\begin{document}

\title{SELP Project 2014/2015}
\subtitle{Tank Scrap - A turn-based strategy game}
\author{Matthew Summers}
\date{\today}

\maketitle

This project proved to be vastly more complex than I had accounted for in the proposal, leading to much less progress being made than I was hoping to make. However, given the complexity, I believe I have produced a reasonably solid implementation of at least the minimum set of requirements outlined in my proposal (and if it deviates from these, I will explain this further into this report), whilst applying good software development practices (for example, good version control discipline, seperation of concerns, and modularity) and for these reasons I believe this project warrants a good mark. In the rest of this report, I will expand on what areas this project deserves a good mark for, what areas contain dubious engineering choices and why I picked them, and what areas are failures and what I have learned from them.

\begin{center}
\rule{6cm}{0.4pt}
\end{center}

As a recap, the aim of this project was to produce a turn-based strategy game (similar to the Advance Wars series of games) in which multiple players could control soldiers and battle with them, with the winner being the player in charge of the last remaining unit. To this end, I produced a web application in Python 3.4 using the {\em Flask} library (and several {\em Flask} extension libraries), {\em SQLAlchemy}, and several other libraries which allows a user to register for an account, log in, create a game with at least one other user as an opponent, and play the previously described game. This was done using {\em Bootstrap} to style a HTML frontend, which on the game screen runs some JavaScript that accesses a JSON API to allow the user to determine valid moves for a given unit, as well as submit those moves to the server for validation and advancing their turn. 

To attempt to enforce code modularity, I started this project by building a directory tree to seperate code into logical units. This was done in the style of an installable Python package, which allowed me to easily specify dependencies, and to include command line administrative tools for the end user. Internally, the package is split into several logical sub-modules: models, for database and ORM models; scripts, for bundled command line tools; security, for cryptography and security helpers; util, for general helpers; tests, self explanatory; and views, for view logic (further split into sub-modules according to view blueprint). Two other submodules exist but these do not contain code, only content files.

This was done to ensure a clean seperation of logical units (for example, model code should not care about display, and so you should find no view code there), allowing developers to easily find what they are looking for in the project. Within views, code was broken into sub-units such that each kind of front-end (JSON API or HTML) had all of it's code grouped together, as each module in these sub-units belongs to a Flask blueprint. There is a sub-unit within the models unit which contains modules relating to the intermediate, in-memory representation of a map. As these are not actually database models, but are related to one, it made sense to put them in a sub-unit.

In terms of content files, the decision to represent units, tiles and maps as external JSON files was done with the aim of seperating data from code - such that content files would not need to be updated with every change in code, and code would not have to be change for every content update. It also has the benefit of allowing content to be created without programming knowledge - which might be useful if this was a group project with less technically minded teammates.

However, choosing to represent content as external files rather than code meant devising a way to import them. Partly for this reason, the management tool was created. It was designed to allow content and users to be added and removed from the database, as well as the ability to create configuration files and blank databases, and run instances from these configurations. This made writing setup scripts for DICE nearly trivial, as well as allowing me to test database models before the frontend view code was ready.

\begin{center}
\rule{6cm}{0.4pt}
\end{center}

Allowing database models to be tested independently of the frontend code proved useful, as a extremely large amount of time was expended trying to determine a suitable database design. The biggest problem encountered lay with representing maps and game state in the database. Maps can be of variable size containing variable numbers of units and features, and describing these elements in a completely fixed, structured data model proved difficult. Ultimately, I opted to store JSON fragments as text within the database - though this is an inelegant solution, it meshes nicely with the content import system described earlier. It would be possible to represent these maps entirely in the database if more tables were added (for map structures and units), but this seemed like an equally inelegant solution as it requires significantly more lookup to construct a map. 

Representing game state proved troublesome also, in that multiple actions can occur in a players turn, and there are multiple turns in a game. My initial idea was to store a sequence of actions associated with a turn, which was associated with a game - allowing me to produce replays, and meaning turn data was immutable, removing possible errors arising from mutability - but this would have required me to store either a delta of the board state or the entire board state every turn, which meant the same serialisation problems as with map data were encountered. I opted to remove the immutability constraint, and store game state in the database as I would in-memory for the sake of progress, and has some nice properties (if each unit is one row, the number of rows cannot exceed the number of tiles on the map, thus bounding the storage requirements).

\begin{center}
\rule{6cm}{0.4pt}
\end{center}

With these problems out of the way, the next problem came with ranking. Initially, I had hoped to use win-loss ratio, but this has undesirable properties (for example, there is no sensible defintion for 0 wins, 0 losses). This was rectified by changing to win rate (percentage of games played that were victories), which does have a sensible definition in the case no games were played (0\% win rate) - but is still open to rigging (such as novices playing one game and quitting, giving them 100\% win rate and putting them at the top; for this reason players with less than 10 played games are not considered for the leaderboard) and should be replaced by a more mathematically rigourous system (such as ELO).

Another dubious area is the JSON API, created to provide data to the JavaScript on the frontend to allow users to make moves. The API is in a seperate module from the HTML-serving frontend, and this seperation of concerns made developing it much easier (it was not necessary to hook into any functionality provided by anything other than the libraries and models). However, the API is insecure (if you know a user's publically visible ID it is possible to submit turns under their name, for example), and this could be rectified by assigning each user a secret key, and requiring them to submit that each turn.

The API also provides access to the functionality which determines valid moves. This is a security risk, as without rate-limiting, it may be possible to perform a denial of service by issuing many requests to this endpoint as it performs an expensive computation, but was done intentionally to avoid replicating code between JavaScript and Python in the interests of improving code reuse (game mechanics were subject to change, and maintaining two implementations would be bothersome) and time efficiency.

On the topic of libraries, my decision to make use of Flask and it's associated extension libraries greatly sped up development, and lead to a clear structure in the project (routes seperated from models, seperated from other code). The use of {\em WTForms} for form input validation saved a lot of tedious busywork, as well as the use of {\em SQLAlchemy} as an ORM to remove the need to hand-write SQL queries and make the project database-agnostic. Furthermore, using {\em Flask-Login} with {\em passlib} to have some reassurance that this is a known-good authentication system, with a known-strong password hashing scheme (PBKDF2-SHA512), removed a lot of debugging work ensuring no obvious side channels existed. Using {\em Bootstrap} to provide a minimal level of user interface polish, as well as a selection of ready-to-go components (some with dynamic elements) was another excellent decision.

As for testing, I feel like my decision to leave that until last was not a good one. Ideally, I should've adopted test driven development, and maintained a fairly good test coverage, rather than having to try and work out what to test at the end, and then running out of time. Several things I would have liked to have tested include the turn submission function and the victory condition check, as well as testing the move validator more comprehensively.

\begin{center}
\rule{6cm}{0.4pt}
\end{center}

With regards to the move validator, it took a sizeable amount of effort to work out how to implement that correct, and there are 3 versions in the code (each slightly differentiated). It should be possible to condense these into two variants (I would have liked to have converted the actual move validator to A*, and left the function which produces a list of valid moves as-is), but due to time this did not happen. However, in general, I feel the majority of my methods are reasonably sized, my code reuse is quite good, and in general my Python is reasonably idiomatic (though some areas are not PEP8-compliant, the majority should be). I also feel that my version control discipline has been very strong - I have endeavoured to adhere strictly to {\em "commit early; commit often"}, attempting to make sure every commit contained only changes that may need to be reverted independently in the event of a mistake.

Additionally, there are a few unfinished features in the project: permissions, unit terrain classes (i.e. air units, sea units, land units), multiple units, and a more dynamic frontend. There exists a permissions model, intended to be used for sectioning off areas of the site between players and administrators (vs the current system of authenticated vs unauthenticated) - indeed, it is possible to specify a permission level when adding via the management tool but it serves no purpose (see the base setup script for an example). 

Furthermore, I was in the process of adding unit terrain classes (tiles are labelled by what unit classes can pass through them, and units are labelled by what tile classes they can pass through) - and if you create a unit file and add it to the game with the management tool with a different combination of these than the tank file, it should be possible to see this in-game to some degree as presently it only affects movement. The next step would be to update the move validator to check these classes to make sure any given unit can attack another unit based on these classes (for example, a submarine shouldn't be able to shoot a tank), and then following that the checks the move validator made should be split into some form of pipeline, to allow easy addition and removal of game rules.

As for the frontend, at present it is mostly-dynamic, requiring only one refresh per turn (to reload the game board and update it's state). I had investigated making the JavaScript update the board automatically, and notify the user of their turn, and to that end created an API endpoint for checking if a given user had control of a game and could input a turn, but was bottlenecked by time. As for the matter of multiple units, the system currently advances the turn after only one of a player's units has moved. It would be a simple matter of adding a {\em moved} column to the {\em Unit} table, and checking that all of a given player's units have moved before advancing the turn - unless the player forcibly advances the turn (which could be achieved with another API endpoint). After this change has been made, maps with multiple units would be playable in a fashion more true to the games which inspired this project.

\begin{center}
\rule{6cm}{0.4pt}
\end{center}

I have for the most part enjoyed working on this project, and as stated before I believe it was significantly more challenging than I had in mind when writing the proposal. Given that, I feel I've done quite well on the project barring a few rough areas and a lack of polish in places. I believe, with the reasons stated above as justification, that I have made a good effort towards implementing this project following solid engineering practices, and for that reason, I think this project warrants a high mark.

\end{document}