Map Import Format
=================

The JSON structure used for importing maps with the management tool is as follows:

- **name**: String naming the map
- **width**: Integer specifying the width of the map in tiles
- **height**: Integer specifying the height of the map in tiles
- **data**: A dictionary of the board data stored in the database, consisting of:
-- **board**: An array of integers. Each element references a tile in the database by ID - can be fiddly
-- **spawn_points**: An array of arrays of dictionaries. Each inner array is associated with a team, for example the team with index 0 is associated with the array at index 0.
--- The dictionaries inside each array are formatted as follows:
---- **unit**: The name of the unit to place
---- **x**: The X coordinate to place it at
---- **y**: The Y coordinate to place it at

This is a temporary format to facilitate setting up the database to allow games to be played in place of a more robust, automated system