Tile Import Format
==================

The JSON structure used by the management tool to import tile types into the database is as follows:

- **name**: The human-readable name of the tile
- **image_path**: The path to the image, relative to the instance's assets directory
- **stationary_vision_modifier**: By how many tiles is a unit's vision enhanced by standing stationary on this tile
- **mobile_vision_modifier**: By how many tiles is a unit's vision enhanced by this tile being in the way
- **movement_modifier**: By how many tiles is a unit's movement affected by this tile being in it's path