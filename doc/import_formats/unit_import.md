Unit Import Format
==================

The JSON structure used by the management tool to import unit types is as follows:

- **name**: The human-readable name of the unit
- **image_path**: The path to the unit image relative to the instance's assets directory. Use {team_colour} to substitute in team colour.
- **vision_range**: The unmodified radius, in tiles, that a unit can see from it's current location
- **max_firing_range**: The furthest away, in tiles, that a unit can strike another unit
- **min_firing_range**: The closest a unit can be, in tiles, for it to be struck by this unit.
- **movement_range**: The unmodified radius, in tiles, that a unit can move from it's current location
- **hp**: How much damage a unit can take before it is destroyed
- **ammo**: How many attacks a unit can make before it needs to be resupplied
- **fuel**: How many turns worth of moves a unit can make before it needs to be resupplied
- **damage**: The unmodified amount of hitpoints this unit will hit for per hitpoint it has
- **armour**: How much damage is blocked when a hit is landed on this unit

The attack formula is very simplistic:

NewHP = OldHP - (AttackerHP*Damage - Armour)