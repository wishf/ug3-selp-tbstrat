tank_scrap
==========

A web-based turn-based strategy game written in Python 3 using Flask for SELP 2014.

Installation
------------

Create a virtual environment with Python 3.4, then simply navigate into the src folder and run `pip install -e .`

An instance can then be created with `tank_scrap init <dbfilename> [configfilename] [-d]`

The `-d` flag enables debug mode, `dbfilename` is required and is simply the name of the file to initialise as a SQLite database and `configfilename` is the name of the file to put the generated configuration into - it defaults to `config.py`.

Under DICE, a script is provided in the setup directory to setup the virtual environment, install the packages and create a basic instance with the sample content imported.

From the repository root, simply run `./setup/dice_install.sh [-d]` to set up the virtual environment and an instance (optionally in debug mode), and then run `. venv/bin/activate` to enter the environment.

4 test accounts are added by default (username - password):
* test - test
* matt - matt
* mari - mari
* admin - admin

Running
-------

Once you have an instance, you can use `tank_scrap run [configfilename]` inside the virtual environment to start the instance defined by the specified configuration file. Again, this defaults to `config.py` in the same directory it is run - thus to run on DICE, once the install script has completed, it is simply a matter of running `tank_scrap run` in the same directory.

Testing
-------

To run the included tests, simply run `nosetests tank_scrap.tests` from within the virtual environment after installation